#!/bin/bash

json=$(</dev/stdin)

curl -k --insecure -X POST \
  https://di0-p51.osisoft.int:5460/ingress/messages \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 156f8ebf-36c2-4ccc-8016-4b7c9b29b463' \
  -H 'action: create' \
  -H 'messageformat: json' \
  -H 'messagetype: data' \
  -H 'omfversion: 1.0' \
  -H 'producertoken: uid=86b914aa-6d0c-411c-8c0a-9259af33dd93&crt=20180807232738868&sig=2yhBdtOt6uZqb7f3Muh+92EWRWz9WvCTcUnAFEYnpmw=' \
  -d ''"$json"''
