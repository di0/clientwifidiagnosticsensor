#!/bin/bash

iwconfig=/sbin/iwconfig

names=()
vals=()

names+=('timestamp')
vals+=(`date -u +"%Y-%m-%dT%H:%M:%SZ"`)

names+=('essid')
vals+=(`$iwconfig wlan0 | sed -n 's/[ "]//g; s/.*ESSID://p'`)

names+=('frequency')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Frequency://p' | cut -d G -f 1`)

names+=('access_point')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Access Point: \(\S*\).*/\1/p'`)

names+=('bit_rate')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Bit Rate=//p' | cut -d M -f 1`)

names+=('tx_power')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Tx-Power=//p' | cut -d d -f 1`)

names+=('link_quality')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Link Quality=//p' | cut -d S -f 1 | bc -l`)

names+=('signal_level')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Signal level=\(\S*\).*/\1/p'`)

names+=('rx_invalid_nwid')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Rx invalid nwid:\(\S*\).*/\1/p'`)

names+=('rx_invalid_crypt')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Rx invalid crypt:\(\S*\).*/\1/p'`)

names+=('rx_invalid_frag')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Rx invalid frag:\(\S*\).*/\1/p'`)

names+=('tx_excessive_retries')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Tx excessive retries:\(\S*\).*/\1/p'`)

names+=('invalid_misc')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Invalid misc:\(\S*\).*/\1/p'`)

names+=('missed_beacon')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Missed beacon:\(\S*\).*/\1/p'`)

json="[{\"containerid\": \"client_wifi_diagnostic_sensor_01\",\"values\": [{"
len=${#names[@]}
for ((i=0;i<$len;++i)); do
    json+="\"${names[i]}\": \"${vals[i]}\""
    if (( i != (len - 1) )) 
      then
        json+=","
    fi
done
json+="}]}]"

echo $json

curl -X POST \
  https://localhost:5000/edge/omf/tenants/default/namespaces/data \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 156f8ebf-36c2-4ccc-8016-4b7c9b29b463' \
  -H 'action: create' \
  -H 'messageformat: json' \
  -H 'messagetype: data' \
  -H 'omfversion: 1.0' \
  -H 'producertoken: LzBzgY1zPnxQ55TX6O4WEcj2i1lfL47fDQM57ectmmDjShQvIsQCbMKbKh4i7be' \
  -d ''"$json"''

