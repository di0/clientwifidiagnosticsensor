#!/bin/bash

json=$(</dev/stdin)
url=$1
token=$(<$2)

curl -k --insecure -X POST \
  "$url"\
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 156f8ebf-36c2-4ccc-8016-4b7c9b29b463' \
  -H 'action: create' \
  -H 'messageformat: json' \
  -H 'messagetype: data' \
  -H 'omfversion: 1.0' \
  -H 'producertoken: '"$token"'' \
  -d ''"$json"''
