#!/bin/bash

json=$(</dev/stdin)

curl -X POST \
  https://localhost:5000/edge/omf/tenants/default/namespaces/data \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 156f8ebf-36c2-4ccc-8016-4b7c9b29b463' \
  -H 'action: create' \
  -H 'messageformat: json' \
  -H 'messagetype: data' \
  -H 'omfversion: 1.0' \
  -H 'producertoken: LzBzgY1zPnxQ55TX6O4WEcj2i1lfL47fDQM57ectmmDjShQvIsQCbMKbKh4i7be' \
  -d ''"$json"''
