#!/bin/bash

date | tee -a /home/pi/bin/log /home/pi/bin/errorlog

/bin/bash /home/pi/bin/foo.sh >> /home/pi/bin/log 2>> /home/pi/bin/errorlog

echo "" | tee -a /home/pi/bin/log /home/pi/bin/errorlog
