#!/bin/bash

json=`/home/pi/bin/buildJson.sh`

# Send to EDS
echo "$json" | /home/pi/bin/toOMF.sh https://localhost:5000/edge/omf/tenants/default/namespaces/data /home/pi/bin/tokens/edsToken

# Send to Connector Relay which forwards to PI
echo "$json" | /home/pi/bin/toOMF.sh https://di0-p51.osisoft.int:5460/ingress/messages /home/pi/bin/tokens/relayToken
