#!/bin/bash

iwconfig=/sbin/iwconfig

names=()
vals=()

names+=('timestamp')
vals+=(`date -u +"%Y-%m-%dT%H:%M:%SZ"`)

names+=('essid')
vals+=(`$iwconfig wlan0 | sed -n 's/[ "]//g; s/.*ESSID://p'`)

names+=('frequency')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Frequency://p' | cut -d G -f 1`)

names+=('accesspoint')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Access Point: \(\S*\).*/\1/p'`)

names+=('bitrate')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Bit Rate=//p' | cut -d M -f 1`)

names+=('txpower')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Tx-Power=//p' | cut -d d -f 1`)

names+=('linkquality')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Link Quality=//p' | cut -d S -f 1 | bc -l | awk '{printf "%f", $1 + $2}'`)

names+=('signallevel')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Signal level=\(\S*\).*/\1/p'`)

names+=('rxinvalidnwid')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Rx invalid nwid:\(\S*\).*/\1/p'`)

names+=('rxinvalidcrypt')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Rx invalid crypt:\(\S*\).*/\1/p'`)

names+=('rxinvalidfrag')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Rx invalid frag:\(\S*\).*/\1/p'`)

names+=('txexcessiveretries')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Tx excessive retries:\(\S*\).*/\1/p'`)

names+=('invalidmisc')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Invalid misc:\(\S*\).*/\1/p'`)

names+=('missedbeacon')
vals+=(`$iwconfig wlan0 | sed -n 's/.*Missed beacon:\(\S*\).*/\1/p'`)

json="[{\"containerid\": \"clientwifidiagnosticsensor01\",\"values\": [{"
len=${#names[@]}
for ((i=0;i<$len;++i)); do
    re='^[-+]?[0-9]*\.?[0-9]*$'
    
    # if is a number
    if [[ ${vals[i]} =~ $re ]] ; then
        json+="\"${names[i]}\": ${vals[i]}"
    else
        json+="\"${names[i]}\": \"${vals[i]}\""
    fi

    if (( i != (len - 1) )) ; then
        json+=","
    fi
done
json+="}]}]"

echo $json
